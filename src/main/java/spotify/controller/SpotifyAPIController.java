package spotify.controller;

import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.Playlist;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import spotify.configuration.AuthorizationConfiguration;
import spotify.model.PlaylistRequest;
import spotify.model.UpdateResponseObject;
import spotify.service.PlaylistModifierService;

@RestController
@RequestMapping(value = "api")
@AllArgsConstructor
public class SpotifyAPIController {

    private PlaylistModifierService playlistModifierService;
    private AuthorizationConfiguration autorizationConfiguration;

    @GetMapping(value = "/test")
    public Paging<PlaylistTrack> hitApi(@RequestBody PlaylistRequest playListId) {
        Playlist playlist = playlistModifierService.processGetPlaylistRequest(playListId.getPlaylistId());
        return playlist.getTracks();
    }

    @GetMapping(value = "/authorize")
    public void authorize() {
        autorizationConfiguration.authorizationCode_Sync();
    }

    @GetMapping(value = "/redirect")
    public void redirectAndConfirmConfig(@RequestParam(value = "code") String code) {
        autorizationConfiguration.finalizeSpotifyConfig(code);
    }

    @GetMapping(value = "/getPlaylists")
    public Paging<PlaylistSimplified> getPlayLists()  {
       return playlistModifierService.processGetAllPlaylists(AuthorizationConfiguration.USER_ID);
    }

    @PostMapping(value = "/update")
    public ResponseEntity<UpdateResponseObject> updatePlaylist() {
        return playlistModifierService.processUpdateMainPlaylist();
    }
}