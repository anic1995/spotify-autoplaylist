package spotify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import spotify.configuration.AuthorizationConfiguration;
import spotify.configuration.DatabaseConfig;
import spotify.controller.SpotifyAPIController;
import spotify.service.PlaylistModifierService;

@SpringBootApplication
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
        DatabaseConfig databaseConfig = new DatabaseConfig();
        PlaylistModifierService playlistModifierService = new PlaylistModifierService(databaseConfig);
        AuthorizationConfiguration authorizationConfiguration = new AuthorizationConfiguration();
        SpotifyAPIController spotifyAPIController = new SpotifyAPIController(playlistModifierService, authorizationConfiguration);
        spotifyAPIController.authorize();
    }
}
