package spotify.service;

import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.Playlist;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;
import com.wrapper.spotify.requests.data.playlists.AddItemsToPlaylistRequest;
import com.wrapper.spotify.requests.data.playlists.GetListOfUsersPlaylistsRequest;
import com.wrapper.spotify.requests.data.playlists.GetPlaylistRequest;
import com.wrapper.spotify.requests.data.playlists.GetPlaylistsItemsRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import spotify.configuration.AuthorizationConfiguration;
import spotify.configuration.DatabaseConfig;
import spotify.model.PlaylistRequest;
import spotify.model.UpdateResponseObject;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class PlaylistModifierService {

    @Value(value = "${spotify.playlist.id.one}")
    private String playlistOneId;
    @Value(value = "${spotify.playlist.id.two}")
    private String playlistTwoId;
    @Value(value = "${spotify.playlist.id.three}")
    private String playlistThreeId;
    @Value(value = "${spotify.playlist.id.main}")
    private String mainPlaylistId;

    private static LocalDateTime lastUpdatedTimeStamp;
    private DatabaseConfig databaseConfig;

    public PlaylistModifierService(DatabaseConfig databaseConfig) {
        this.databaseConfig = databaseConfig;
    }

    public Playlist processGetPlaylistRequest(String playlistId) {
        GetPlaylistRequest getPlaylistRequest = AuthorizationConfiguration.spotifyApi
                .getPlaylist(playlistId)
                .build();
        try {
            final CompletableFuture<Playlist> playlistFuture = getPlaylistRequest.executeAsync();
            return playlistFuture.join();
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    public Paging<PlaylistSimplified> processGetAllPlaylists(String userId) {
        GetListOfUsersPlaylistsRequest listOfUsersPlaylists = AuthorizationConfiguration.spotifyApi
                .getListOfUsersPlaylists(userId)
                .build();
        try {
            final CompletableFuture<Paging<PlaylistSimplified>> playlistCompletableFuture = listOfUsersPlaylists.executeAsync();
            return playlistCompletableFuture.join();
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    public ResponseEntity<UpdateResponseObject> processUpdateMainPlaylist() {
        List<PlaylistRequest> playlists = generateSourcePlaylists();
        UpdateResponseObject updateResponseObject = new UpdateResponseObject();
        lastUpdatedTimeStamp = getLastUpdatedTimeStamp();
        boolean success = false;
        Playlist mainPlaylist = processGetPlaylistRequest(mainPlaylistId);
        for (PlaylistRequest playlist : playlists) {
            if (playlist.getEnable()) {
                success = addSongsToNewPlaylist(processGetPlaylistRequest(playlist.getPlaylistId()), playlist.getPlaylistId(), mainPlaylist);
                if (!success) {
                    break;
                }
            }
        }
        updateDatabaseTimeStamp();
        if (!success) {
            updateResponseObject.setResponse("Could not add all new songs to playlist");
            updateResponseObject.setSuccess(false);
            return new ResponseEntity<>(updateResponseObject, HttpStatus.BAD_REQUEST);
        } else {
            updateResponseObject.setResponse("Successfully added all new songs to playlist");
            updateResponseObject.setSuccess(true);
            return new ResponseEntity<>(updateResponseObject, HttpStatus.OK);
        }
    }

    private Boolean addSongsToNewPlaylist(Playlist playlist, String playlistId, Playlist mainplaylist) {
        List<String> tempSongList = new ArrayList<>();

        List<PlaylistTrack> consumedTracks = Arrays.asList(playlist.getTracks().getItems());

        Integer totalTracks = playlist.getTracks().getTotal();
        int processedTracks = 0;

        //deals with 100 song list limit
        if (totalTracks >= 100) {
            while (processedTracks < totalTracks) {
                processedTracks = processedTracks + consumedTracks.size();
                GetPlaylistsItemsRequest items = AuthorizationConfiguration.spotifyApi
                        .getPlaylistsItems(playlistId)
                        .offset(processedTracks)
                        .build();
                try {
                    Paging<PlaylistTrack> execute = items.execute();
                    List<PlaylistTrack> playlistTracks = Arrays.asList(execute.getItems());
                    playlistTracks
                            .stream()
                            .filter(this::checkForSongIsWithinTimeFrame)
//                            .filter(track -> removeDuplicates(track, mainplaylist))
                            .forEach(track -> addTrackToTemporaryList(tempSongList, track, mainplaylist));
                } catch (Exception e) {
                    throw new IllegalArgumentException();
                }
            }
        }
        consumedTracks
                .stream()
                .filter(this::checkForSongIsWithinTimeFrame)
//                .filter(track -> removeDuplicates(track, mainplaylist))
                .forEach(track -> addTrackToTemporaryList(tempSongList, track, mainplaylist));

        String[] finalArray = tempSongList.toArray(new String[tempSongList.size()]);
        return addItemsToPlaylist_Sync(finalArray, mainPlaylistId);
    }

    private Boolean checkForSongIsWithinTimeFrame(PlaylistTrack playlistTrack) {
        long oldTimeStamp = Timestamp.valueOf(lastUpdatedTimeStamp).getTime();
        return playlistTrack.getAddedAt().getTime() > oldTimeStamp;
    }

    private Boolean checkForDuplicate(PlaylistTrack playlistTrack, Playlist mainPlaylist) {
        List<PlaylistTrack> mainPlaylistTracks = Arrays.asList(mainPlaylist.getTracks().getItems());

        return mainPlaylistTracks
                .stream()
                .map(PlaylistTrack::getTrack)
                .anyMatch(mainTrack -> mainTrack.getId().equals(playlistTrack.getTrack().getId()));
    }

    private void updateDatabaseTimeStamp() {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = databaseConfig.getConnection();
            preparedStatement = connection.prepareStatement("UPDATE spotify.playlist_timestamp SET timestamp = ? WHERE Project = ?");
            preparedStatement.setString(1, LocalDateTime.now().toString());
            preparedStatement.setString(2, "autoplaylist");
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private LocalDateTime getLastUpdatedTimeStamp() {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = databaseConfig.getConnection();
            preparedStatement = connection.prepareStatement(
                    "SELECT * FROM spotify.playlist_timestamp WHERE Project = ?");
            preparedStatement.setString(1, "autoplaylist");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                return resultSet.getTimestamp("timestamp").toLocalDateTime();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                preparedStatement.close();
                resultSet.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void addTrackToTemporaryList(List<String> songList, PlaylistTrack track, Playlist mainPlaylist) {
        String trackURI = track.getTrack().getUri();
        if (checkForDuplicate(track, mainPlaylist)) {
            return;
        }
        songList.add(trackURI);
    }

    private List<PlaylistRequest> generateSourcePlaylists() {
        List<PlaylistRequest> playlistRequests = new ArrayList<>();
        PlaylistRequest playlistRequest = new PlaylistRequest();
        playlistRequest.setPlaylistId(playlistOneId);
        playlistRequest.setEnable(true);
        playlistRequests.add(playlistRequest);

        PlaylistRequest playlistRequest2 = new PlaylistRequest();
        playlistRequest2.setPlaylistId(playlistTwoId);
        playlistRequest2.setEnable(true);
        playlistRequests.add(playlistRequest2);

        PlaylistRequest playlistRequest3 = new PlaylistRequest();
        playlistRequest3.setPlaylistId(playlistThreeId);
        playlistRequest3.setEnable(true);
        playlistRequests.add(playlistRequest3);

        return playlistRequests;
    }

    private Boolean addItemsToPlaylist_Sync(String[] songsToAdd, String mainPlaylistId) {
        try {
            if (songsToAdd.length > 0) {
                AddItemsToPlaylistRequest addItemsToPlaylistRequest = AuthorizationConfiguration.spotifyApi
                        .addItemsToPlaylist(mainPlaylistId, songsToAdd)
                        .build();
                addItemsToPlaylistRequest.execute();
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}