package spotify.configuration;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.SpotifyHttpManager;
import com.wrapper.spotify.model_objects.credentials.AuthorizationCodeCredentials;
import com.wrapper.spotify.requests.authorization.authorization_code.AuthorizationCodeRequest;
import com.wrapper.spotify.requests.authorization.authorization_code.AuthorizationCodeUriRequest;
import org.springframework.context.annotation.Configuration;

import java.net.URI;

@Configuration
public class AuthorizationConfiguration {

    private static String CLIENT_ID = "2754f00db3e0455baf7bb793bec3846c";
    private static String CLIENT_SECRET = "8196f320606c49a0aa2594700f369a2e";
    private static URI redirectUri = SpotifyHttpManager.makeUri("http://localhost:8081/api/redirect");
    public static String USER_ID = "anic1995";
    public static String SCOPE_PRIVATE = "playlist-modify-private";
    public static String SCOPE_PUBLIC = "playlist-modify-public";

    public static final SpotifyApi spotifyApi = new SpotifyApi.Builder()
            .setClientId(CLIENT_ID)
            .setClientSecret(CLIENT_SECRET)
            .setRedirectUri(redirectUri)
            .build();

    public void authorizationCode_Sync() {
        try {
            AuthorizationCodeUriRequest build = spotifyApi.authorizationCodeUri()
                    .scope(SCOPE_PRIVATE)
                    .scope(SCOPE_PUBLIC)
                    .build();
            URI execute = build.execute();
            System.out.println("URI: " + execute.toString());

            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec("rundll32 url.dll,FileProtocolHandler " + execute.toString());
            } catch (Exception e) {
                throw new IllegalArgumentException();
            }
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    public void finalizeSpotifyConfig(String accessCode) {
        try {
            AuthorizationCodeRequest authorizationCodeRequest = spotifyApi.authorizationCode(accessCode)
                    .build();
            final AuthorizationCodeCredentials authorizationCodeCredentials = authorizationCodeRequest.execute();
            spotifyApi.setAccessToken(authorizationCodeCredentials.getAccessToken());
            spotifyApi.setRefreshToken(authorizationCodeCredentials.getRefreshToken());
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
 }