package spotify.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Configuration
public class DatabaseConfig {

    @Value("${mysql.root.user}")
    private String username;
    @Value("${mysql.root.password}")
    private String password;
    @Value("${mysql.root.driver}")
    private String driver;
    @Value("${mysql.root.url}")
    private String url;

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }
}
