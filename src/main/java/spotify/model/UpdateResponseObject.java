package spotify.model;

import lombok.Data;

@Data
public class UpdateResponseObject {
    Boolean success;
    String response;
}
