package spotify.model;

import lombok.Data;

@Data
public class PlaylistRequest {
    private String playlistId;
    private Boolean enable;
}
